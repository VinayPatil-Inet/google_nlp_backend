

const Pool = require('pg').Pool
const pool = new Pool({
  user: "postgres",
  host: 'localhost',
  database: "google_health_nlp",
  password: 'admin123',
  port: 5432,
})
pool.connect(function(err,res) {
  if (err) {
    return console.error('error: ' + err.message);

  }

  console.log('Connected to the postgres sql.');
});
module.exports= pool;

