var pool = require('../config/db.config')
const path = require('path');
const fs = require('fs');
const util = require('util');
const { google } = require('googleapis');
const fetch = require('node-fetch');
var bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');
var ACCESS_TOKEN_SECRET = 'google'
var jsonexport = require('jsonexport');





const getData = (request, res) => {
    var genarateCsvFile = request.params.genarateCsvFile
    var sql = `SELECT id,file_name,response_json FROM files_details`
    pool.query(sql, (error, results) => {
        if (error) {
            throw error
        }
        var data = results.rows

        res.status(200).json({ body: 'Success', data })
        console.log(data, "data")
        console.log(genarateCsvFile,"genarateCsvFile")
        if(genarateCsvFile == 'true'){
           // var data = results.row
            var arrayResult = results.rows
            var filterFinalEntity = [];
            arrayResult.map((x) => {
                var final_res = x.response_json
                //   console.log(final_res, "final_res");
                var resultEntities = final_res.entities
                //  console.log(resultEntities, "resultEntities");
                var filteredEntity = [];
                resultEntities.map(filteredName => {
                    var filteredpnames;
                    var filteredICD = [];
                    filteredName.vocabularyCodes.filter(name => name.includes('ICD10CM')).map(filteredNames => {
                        // console.log(filteredNames,filteredName.preferredTerm, "filteredNames")
                        filteredpnames = filteredName.preferredTerm
                        filteredICD.push(filteredNames)
                    })
                    if (filteredpnames !== undefined) {
                        filteredEntity.push({ "preferredTerm": filteredpnames, "CodeTypeICD10": "ICD10", "ICDCode": filteredICD })
                        // console.log(filteredICD, "filteredICD")
                    }
                })
                resultEntities.map(filteredName => {
                    var filteredpnamesLNC;
                    var filteredLNC = [];
                    filteredName.vocabularyCodes.filter(name => name.includes('LNC')).map(filteredNames => {
                        // console.log(filteredNames,filteredName.preferredTerm, "filteredNames")
                        filteredpnamesLNC = filteredName.preferredTerm
                        filteredLNC.push(filteredNames)
                    })
                    if (filteredpnamesLNC !== undefined) {
                        filteredEntity.push({ "preferredTerm": filteredpnamesLNC, "CodeTypeLIONC": "LIONC", "ICDCode": filteredLNC })
                        //  console.log(filteredLNC, "filteredLNC")
                    }
                })
                //Snomed Codes
                resultEntities.map(filteredName => {
                    var filteredpnamesSnomed;
                    var filteredSnomed = [];
                    filteredName.vocabularyCodes.filter(name => name.includes('SNOMEDCT')).map(filteredNames => {
                        // console.log(filteredNames,filteredName.preferredTerm, "filteredNames")
                        filteredpnamesSnomed = filteredName.preferredTerm
                        filteredSnomed.push(filteredNames)
                    })
                    if (filteredpnamesSnomed !== undefined) {
                        filteredEntity.push({ "preferredTerm": filteredpnamesSnomed,   "CodeTypeSNOMED": "SNOMED", "ICDCode": filteredSnomed })
                        //  console.log(filteredSnomed, "filteredSnomed")
                    }
                })
                filterFinalEntity.push({
                    file_name: x.file_name,
                    filteredEntity: filteredEntity,
                })
               var data = []
                filterFinalEntity.forEach(item => {
                    data.push({
                        FileName: item.file_name,
                        preferredTerm :'',
                        ICDCode: ''

                    });
                    for (var i = 0; i < item.filteredEntity.length; i++) {
                        const role = item.filteredEntity[i];
                        data.push({
                            FileName: '' ,
                            CodeType: role.CodeTypeICD10 || role.CodeTypeLIONC  || role.CodeTypeSNOMED ,
                            preferredTerm: role.preferredTerm  ||  role.preferredTermLNC || role.preferredTermSnomed,
                            ICDCode: role.ICDCode || role.LNCCode || role.SnomedCode,
                        });
                    }
                });
                jsonexport(data, function (err, csv) {
                    if (err) return console.log(err);
                   // console.log(csv);
                    fs.writeFile("./OutputFiles/finalOutput.csv", csv, 'utf8', function (err) {
                        if (err) {
                            console.log("An error occured while writing JSON Object to File.");
                            return console.log(err);
                        }
                        console.log("CSV file has been saved.");
                    });
                });
            })

        }


    })
}
const getIdByFile = (req, res) => {

    const id = parseInt(req.params.id)

    var sqlQuery = `SELECT * from files_details where id  = ${id}`

    // console.log('sqlquery=', sqlQuery)
    pool.query(sqlQuery, function (error, results) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        } else {
            var data = results.rows

            res.status(200).json({ body: 'Success', data })


        }

    })
}

async function getFiles() {
    const dirname = path.join('files/');
    const readdir = util.promisify(fs.readdir);
    const readFile = util.promisify(fs.readFile);
    try {
        const filenames = await readdir(dirname);
        // console.log({ filenames });
        const files_promise = filenames.map(filename => {
            return readFile(dirname + filename, 'utf-8');
        });
        const response = await Promise.all(files_promise);

        let final_array = []
        //  console.log({ response })
        //  return response
        return filenames.reduce((accumlater, filename, currentIndex) => {
            const text = response[currentIndex];
            //  console.log(currentIndex, "currentIndex");

            final_array.push({
                "filename": filename,
                text
            });
            //  console.log(final_array, "final_array");


            return final_array
        }, {});
    } catch (error) {
        console.error(error);
    }

}
const postText = async (req, res) => {

    let resultText = await getFiles()

    console.log(resultText, 'data_pppppppppppp');
    console.log(resultText[0].filename, 'filename');
    console.log(resultText[0].text, 'text');

    for (var i = 0; i < resultText.length; i++) {
        console.log(resultText[i], 'resultText');


        const url = 'https://healthcare.googleapis.com/v1/projects/upbeat-math-349812/locations/us-central1/services/nlp:analyzeEntities?key=cee84a11f2d4cf4314161c8d7497279e5ca86676';



        const auth = new google.auth.GoogleAuth({
            keyFile: './key.json',
            scopes: ['https://www.googleapis.com/auth/cloud-healthcare'],
        });

        const accessToken = await auth.getAccessToken();

        const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify({
                'document_content': resultText[i].text,

                licensedVocabularies: [
                    "LICENSED_VOCABULARY_UNSPECIFIED",
                    "ICD10CM", "SNOMEDCT_US"
                ]
            }),
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        });
        let entityJson = await response.json();
        var data = entityJson

        var final_response_json = JSON.stringify(data)



        //console.log(final_response_json, "final_response_json")


        var sqlQuery = `INSERT INTO files_details (file_name,text,response_json)
         VALUES ('${resultText[i].filename}','${resultText[i].text}','${final_response_json}')`
        console.log(sqlQuery, "sqlQuery")
        pool.query(sqlQuery, (error, results) => {
            if (error) {
                throw error
            }
        })

    }
    res.status(200).json({ body: 'Success' })


}

const login = (req, res) => {
    const { email, password } = req.body
    var sql = `SELECT * from users where users.email = '${email}'`
    console.log('sql=', sql)
    pool.query(sql, function (error, recordset) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        }
        else {
            var data = recordset.rows
            if (data.length == 0) {
                console.log("User does not exist")
                res.status(404).send({ status_code: 404, message: "User does not exist" })
            }

            else {

                console.log(password, " bcrypt password ", data[0].password)
                let compare = bcrypt.compareSync(password, data[0].password)
                var user_id = data[0].user_id
                console.log(user_id)
                const token = jwt.sign(
                    {
                        email: data[0].email,
                        user_id: data[0].user_id
                    },
                    ACCESS_TOKEN_SECRET,
                    {
                        expiresIn: "2h",
                    }
                );
                data[0].token = token;

                if (compare == false) {
                    console.log("Email and password does not match")
                    res.status(400).send({ status_code: 400, message: "Email and password does not match" })
                }
                else {

                    res.status(200).json({
                        status_code: "200", message: 'successfully authenticated', data
                    })
                }

            }
        }

    })
}

const createUser = (req, res) => {
    const { name, email, password, phone } = req.body
    var sql = `select * from users where email ='${email}' `
    console.log('sql=', sql)
    pool.query(sql, function (error, recordset) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        }
        else {
            var results = recordset.rows
            if (results[0]) {
                console.log("Email already exits")
                res.status(404).send({ "code": 404, message: "Email already exits" })
            }
            else {
                console.log(results, "user")
                pool.connect(function () {
                    bcrypt.hash(password, 10, function (err, hash) {
                        var sqlQuery = `INSERT INTO users (name,email, password, phone)
                        values ('${name}','${email}','${hash}', '${phone}') RETURNING *`
                        console.log(sqlQuery, "users")
                        pool.query(sqlQuery, (error, results) => {
                            if (error) {
                                throw error
                            }
                            var data = results.rows

                            res.status(200).json({ body: 'successfully created a user', data })
                        });

                    })
                })
            }

        }

    })

}
const forgotPassword = (req, res) => {
    var email = req.body.email;


    console.log('user = ', email);


    var mail = {
        email: email,
    }
    JWT_ACC_ACTIVATE = 'accountactivatekey123'
    // secret_code = sha1(234567823456789)
    // console.log(secret_code, "secret_code")


    var sqlQuery = `SELECT * FROM users where email = '${email}' `
    console.log('sqlquery=', sqlQuery)

    pool.query(sqlQuery, function (error, recordset) {
        if (error) {
            res.send({
                "code": 400,
                message: 'there are some error with query'
            })
        }
        else {
            var results = recordset.rows
            var user_id = recordset.rows[0].user_id
            console.log("user_id", user_id)
            const token_mail_verification = jwt.sign(mail, JWT_ACC_ACTIVATE, { expiresIn: '50min' });

            var url = `http://localhost:3000/ResetPassword?email=${email}` + token_mail_verification;
            // var url = 'https://payerhub.health-chain.io' + `/ResetPassword?email=${email}` + token_mail_verification;



            if (!results[0]) {
                console.log("user not found")
                res.status(404).send({ "code": 404, message: "Email does not exits" })
            }
            else {
                // var mailOptions = {
                //     from: 'developer@health-chain.io',
                //     to: email,
                //     subject: 'Email ResetPassword - Healthchain.io',
                //     text: "Click on the link below to veriy your account " + url,

                // };
                const token = jwt.sign({ email }, JWT_ACC_ACTIVATE, { expiresIn: '1day' })

                var mailOptions = {
                    // from: 'developer@health-chain.io',
                    // name:'HealthChain',
                    from: {
                        email: 'developer@health-chain.io',
                        name: 'HealthChain'
                    },
                    to: email,
                    subject: 'Email ResetPassword - Healthchain.io',
                    text: "Click on the link below to veriy your account " + url,
                    html: 'Please click on the below button to verify your account <br><br><a href="' + url + '" style="background-color: #f44336;color: white;padding: 14px 25px;text-align: center;text-decoration: none;display: inline-block;">Verify Account</a> ',
                    //   html: 'Please click on the below button to verify your account <br><br><a href="' + url + '" style="background-color: #f44336;color: white;padding: 14px 25px;text-align: center;text-decoration: none;display: inline-block;">Verify Account</a> ',

                };
                sgMail.send(mailOptions, function (error, info) {
                    if (error) {
                        console.log("not send email")
                        return 1
                    } else {
                        console.log("send email success")
                        return 0
                    }
                });

                var data = results.rows
                res.status(200).json({ body: 'The reset password link has been sent to your email address', data })



            }

        }

    })
}
const ResetPassword = async (req, res) => {
    try {
        var params = req.body;
        var Query = `select * from users  WHERE email = '${params.email}'`
        console.log("Query:", Query)
        pool.query(Query, function (err, result) {
            if (err) {
                console.log("Error:", err)
                var obj = {
                    Status: 400,
                    message: err.message
                }
                res.json(obj)

            }
            else {
                var userData = {};

                if (result.rows && result.rows.length != 0) {
                    userData = result.rows[0];
                    userEmail = result.rows[0].email
                    console.log(userEmail, "userEmail")
                    //  var object_id = result.rows[0].object_id
                    console.log(userData, "userData")
                    var hashedPassword = bcrypt.hashSync(params.password, 8);
                    var Query2 = `UPDATE users SET password = '${hashedPassword}'  WHERE email = '${userEmail}'`;
                    console.log("Query2", Query2);

                    pool.query(Query2, function (err, result) {
                        if (err) {
                            var obj = {
                                Status: 400,
                                message: err.message
                            }
                            res.json(obj)
                        }
                        else {
                            //   getToken(object_id, hashedPassword);
                            var obj = {
                                Status: 200,
                                message: "Password changed successfully",

                            }
                            res.json(obj)
                        }
                    })
                }
                else {
                    var obj = {
                        Status: 400,
                        message: "User did not matched"
                    }
                    res.json(obj)
                }


            }
        })

    }
    catch (err) {
        var obj = {
            Status: 400,
            message: err.message
        }
        res.json(obj)
        console.log("Error:", err)
    }

}
// const finalOutput =  (req, res) => {
//     //console.log(req.body,"padma")

//     var ex = req.body
//     var final = JSON.stringify(ex)
//   //  console.log(final, "padma")

//     // fs.writeFile("output.csv", final, 'utf8', function (err) {
//     //     if (err) {
//     //         console.log("An error occured while writing JSON Object to File.");
//     //         return console.log(err);
//     //     }
//     //     console.log("JSON file has been saved.");
//     // });
//     // jsonexport(someData,function(err, csv){
//     //     if(err) return console.log(err);
//     //     console.log(csv);
//     //         fs.writeFile("finalOutput.csv", csv, 'utf8', function (err) {
//     //     if (err) {
//     //         console.log("An error occured while writing JSON Object to File.");
//     //         return console.log(err);
//     //     }
//     //     console.log("JSON file has been saved.");
//     // });
//     // });

// }












module.exports = {
    forgotPassword,
    ResetPassword,
    createUser,
    getData,
    getFiles,
    postText,
    getIdByFile,
    login,
    //  finalOutput


}










