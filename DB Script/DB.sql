CREATE TABLE users
(
    user_id serial NOT NULL,
	name text,
    phone text,
    email text,
    password text,
    inserted_date varchar(15) default to_char(CURRENT_DATE, 'dd-mm-yyyy'),
	updated_date varchar(15) default to_char(CURRENT_DATE, 'dd-mm-yyyy'),
    PRIMARY KEY (user_id)
);

CREATE TABLE files_details
(
    id integer NOT NULL,
    file_name text,
    text text ,
    inserted_date varchar(15) default to_char(CURRENT_DATE, 'dd-mm-yyyy'),
	updated_date varchar(15) default to_char(CURRENT_DATE, 'dd-mm-yyyy'),
    response_json jsonb,
   PRIMARY KEY (id)
)